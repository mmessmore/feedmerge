============
Installation
============

At the command line either via easy_install or pip::

    $ easy_install feedmerge
    $ pip install feedmerge

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv feedmerge
    $ pip install feedmerge
