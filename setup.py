#!/usr/bin/env python


try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


readme = open('README.rst').read()
history = open('HISTORY.rst').read().replace('.. :changelog:', '')
requirements = open('requirements.txt').read().splitlines()

setup(
    name='feedmerge',
    version='0.1.0',
    description='Merges RSS feeds',
    long_description=readme + '\n\n' + history,
    author='Mike Messmore',
    author_email='mike@messmore.org',
    url='https://github.com/mmessmore/feedmerge',
    packages=[
        'feedmerge',
    ],
    package_dir={'feedmerge': 'feedmerge'},
    include_package_data=True,
    install_requires=requirements,
    license='MIT',
    zip_safe=False,
    keywords='feedmerge',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: Implementation :: PyPy',
    ],
)
